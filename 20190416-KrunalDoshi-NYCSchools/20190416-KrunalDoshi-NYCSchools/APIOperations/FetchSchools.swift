//
//  File.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

//This class has the Single Responsibility to create the request for fetching the school data and parsing the school data and converting the raw data in the Model Class.

import Foundation
import SwiftyJSON

class FetchSchools: JSONOperation<[School]> {
    override init() {
        super.init()
        self.request = Request(method: .get, endpoint: "/s3k6-pzi2.json?state_code=NY&$select=school_name,dbn,website,school_email")
        self.onParseResponse = { json in
            return School.load(list: json.arrayValue)
        }
    }
}
