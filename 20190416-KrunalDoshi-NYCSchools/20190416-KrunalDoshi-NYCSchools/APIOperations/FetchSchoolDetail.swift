//
//  FetchSchoolDetail.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

//This class has the Single Responsibility to create the request for fetching the SAT Scores for school  and parsing that data and converting the raw data in the Model Class.

import Foundation

class FetchSchoolDetail: JSONOperation<SchoolDetail> {
    init(dbn: String) {
        super.init()
        self.request = Request(method: .get, endpoint: "/f9bf-2cp4.json?dbn={id}", params: ["id":dbn])
        self.onParseResponse = { json in
            return SchoolDetail(json)
        }
    }
}
