//
//  SchoolDetailViewController.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    
    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var lblReadingAvgScore: UILabel!
    @IBOutlet weak var lblWritingAvgScore: UILabel!
    @IBOutlet weak var lblMathAvgScore: UILabel!
    @IBOutlet weak var lblSchoolWebSite: UILabel!
    @IBOutlet weak var lblSchoolEmail: UILabel!
    
    var school: School?
    var schoolDetail: SchoolDetail?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "School Details"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchSchoolDetail()
    }
    
    /// This Function will fetch the SAT scores for school
    func fetchSchoolDetail() {
        //1. Check the school object is present or not
        guard let school = self.school else {
            return
        }
        //2. Create the service Config
        let config = ServiceConfig.appConfig()
        let service = Service(config!)
        //3. Fetch the School Detail
        //Given more time this can be wrapped in the VIPER Model and the below code will go into the PRESENTER class which in turn calls the Interactor class to call the API.
        FetchSchoolDetail(dbn: school.id).execute(in: service).then{ [weak self] schoolDetail in
            self?.schoolDetail = schoolDetail
            self?.updateSchoolDetails()
        }.catch( { err in
            
        })
    }
    
    /// This function will update the UI based on the data received from the API.
    func updateSchoolDetails() {
        guard let school = self.school, let detail = schoolDetail else {
            return
        }
        self.lblSchoolName.text = school.name
        self.lblReadingAvgScore.text = detail.criticalReadingAvgScore.isEmpty ? "NA" : detail.criticalReadingAvgScore
        self.lblWritingAvgScore.text = detail.writingAvgScore.isEmpty ? "NA" : detail.writingAvgScore
        self.lblMathAvgScore.text = detail.mathAvgScore.isEmpty ? "NA" : detail.mathAvgScore
        self.lblSchoolWebSite.text = "WebSite: \(school.website)"
        self.lblSchoolEmail.text = "Email: \(school.email)"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
