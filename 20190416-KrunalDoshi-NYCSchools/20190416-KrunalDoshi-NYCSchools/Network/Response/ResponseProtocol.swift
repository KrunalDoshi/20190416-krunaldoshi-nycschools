//
//  ResponseProtocol.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

import Foundation

import SwiftyJSON
import Alamofire

protocol ResponseProtocol {
    
    /// Type of response (success or failure)
    var type: Response.Result { get }
    
    /// Request
    var request: RequestProtocol { get }
    
    /// Return the http url response
    var httpResponse: HTTPURLResponse? { get }
    
    /// Return HTTP status code of the response
    var httpStatusCode: Int? { get }
    
    /// Return the raw Data instance response of the request
    var data: Data? { get }
    
    /// Attempt to decode Data received from server and return a JSON object.
    /// If it fails it will return an empty JSON object.
    /// Value is stored internally so subsequent calls return cached value.
    ///
    /// - Returns: JSON
    func toJSON() -> JSON?
    
    /// Attempt to decode Data received from server and return a String object.
    /// If it fails it return `nil`.
    /// Call is not cached but evaluated at each call.
    /// If no encoding is specified, `utf8` is used instead.
    ///
    /// - Parameter encoding: encoding of the data
    /// - Returns: String or `nil` if failed
    func toString(_ encoding: String.Encoding?) -> String?
    
}

class Response: ResponseProtocol {
    
    /// Type of response
    ///
    /// - success: success
    /// - error: error
    enum Result {
        case success(_: Int)
        case error(_: Int)
        case noResponse
        
        private static let successCodes: Range<Int> = 200..<299
        
        static func from(response: HTTPURLResponse?) -> Result {
            guard let r = response else {
                return .noResponse
            }
            return (Result.successCodes.contains(r.statusCode) ? .success(r.statusCode) : .error(r.statusCode))
        }
        
        var code: Int? {
            switch self {
            case .success(let code):     return code
            case .error(let code):        return code
            case .noResponse:            return nil
            }
        }
    }
    
    /// Type of result
    let type: Response.Result
    
    /// Status code of the response
    var httpStatusCode: Int? {
        return self.type.code
    }
    
    /// HTTPURLResponse
    let httpResponse: HTTPURLResponse?
    
    /// Raw data of the response
    var data: Data?
    
    /// Request executed
    let request: RequestProtocol
    
    /// Initialize a new response from Alamofire response
    ///
    /// - Parameters:
    ///   - response: response
    ///   - request: request
    init(afResponse response: DefaultDataResponse, request: RequestProtocol) {
        self.type = Result.from(response: response.response)
        self.httpResponse = response.response
        self.data = response.data
        self.request = request
    }
    
    public func toJSON() -> JSON? {
        return self.cachedJSON
    }
    
    public func toString(_ encoding: String.Encoding? = nil) -> String? {
        guard let d = self.data else { return nil }
        return String(data: d, encoding: encoding ?? .utf8)
    }
    
    private lazy var cachedJSON: JSON? = {
        do {
            return try JSON(data: self.data ?? Data())
        }catch {
            print("Error Converting data to json")
        }
        return nil
    }()
    
}
