//
//  Request.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

import Foundation
import SwiftyJSON
import Hydra

class Request: RequestProtocol {
    var context: Context?
    
    /// Endpoint for request
    var endpoint: String
    
    /// Body of the request
    var body: RequestBody?
    
    /// HTTP method of the request
    var method: RequestMethod?
    
    /// Fields of the request
    var fields: ParametersDict?
    
    /// URL of the request
    var urlParams: ParametersDict?
    
    /// Headers of the request
    var headers: HeadersDict?
    
    /// Cache policy
    var cachePolicy: URLRequest.CachePolicy?
    
    /// Timeout of the request
    var timeout: TimeInterval?
    
    /// Initialize a new request
    ///
    /// - Parameters:
    ///   - method: HTTP Method request (if not specified, `.get` is used)
    ///   - endpoint: endpoint of the request
    ///   - params: paramters to replace in endpoint
    ///   - fields: fields to append inside the url
    ///   - body: body to set
    init(method: RequestMethod = .get, endpoint: String = "", params: ParametersDict? = nil, fields: ParametersDict? = nil, body: RequestBody? = nil) {
        self.method = method
        self.endpoint = endpoint
        self.urlParams = params
        self.fields = fields
        self.body = body
    }
}
