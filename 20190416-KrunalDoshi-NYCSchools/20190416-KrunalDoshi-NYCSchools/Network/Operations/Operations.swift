//
//  Operation.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

import Foundation
import SwiftyJSON
import Hydra

/// Data operation, return a response via Promise
class DataOperation<ResponseProtocol>: OperationProtocol {
    typealias T = ResponseProtocol
    
    /// Request to send
    var request: RequestProtocol?
    
    /// Init
    init() { }
    
    /// Execute the request in a service and return a promise with server response
    ///
    /// - Parameters:
    ///   - service: service to use
    ///   - retry: retry attempts in case of failure
    /// - Returns: Promise
    func execute(in service: ServiceProtocol, retry: Int? = nil) -> Promise<ResponseProtocol> {
        return Promise<ResponseProtocol>({ (r, rj, s) in
            guard let rq = self.request else { // missing request
                rj(NetworkError.missingEndpoint)
                return
            }
            // execute the request
            service.execute(rq, retry: retry).then({ dataResponse in
                let x: ResponseProtocol = dataResponse as! ResponseProtocol
                r(x)
            }).catch(rj)
        })
    }
}


/// JSON Operation, return a response as JSON
class JSONOperation<Output>: OperationProtocol {
    
    typealias T = Output
    
    /// Request
    var request: RequestProtocol?
    
    /// Implement this function to provide a parsing from raw JSON to `Output`
    var onParseResponse: ((JSON) throws -> (Output))? = nil
    
    /// Init
    init() {
        self.onParseResponse = { _ in
            fatalError("You must implement a `onParseResponse` to return your <Output> from JSONOperation")
        }
    }
    
    /// Execute a request and return your specified model `Output`.
    ///
    /// - Parameters:
    ///   - service: service to use
    ///   - retry: retry attempts
    /// - Returns: Promise
    func execute(in service: ServiceProtocol, retry: Int? = nil) -> Promise<Output> {
        return Promise<Output>({ (r, rj, s) in
            guard let rq = self.request else { // missing request
                rj(NetworkError.missingEndpoint)
                return
            }
            // execute the request
            service.execute(rq, retry: retry).then({ response in
                if let json = response.toJSON() {
                    do {
                        // Attempt to call custom parsing. Your own class must implement it.
                        let parsedObj = try self.onParseResponse!(json)
                        r(parsedObj)
                    }
                    catch {
                        throw NetworkError.failedToParseJSON(json,response)
                    }
                } else {
                    rj(NetworkError.noResponse(response))
                }
            }).catch(rj)
        })
    }
}

