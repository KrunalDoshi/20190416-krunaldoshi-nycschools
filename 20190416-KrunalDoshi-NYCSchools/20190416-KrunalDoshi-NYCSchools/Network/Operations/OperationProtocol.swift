//
//  OperationProtocol.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

import Foundation
import Hydra

/// Operation Protocol
protocol OperationProtocol {
    associatedtype T
    
    /// Request
    var request: RequestProtocol? { get set }
    
    /// Execute an operation into specified service
    ///
    /// - Parameters:
    ///   - service: service to use
    ///   - retry: retry attempts
    /// - Returns: Promise
    func execute(in service: ServiceProtocol, retry: Int?) -> Promise<T>
    
}

