//
//  SchoolTableViewCell.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    var school: School?
    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup() {
        guard let school = self.school else {
            return
        }
        lblName.text = school.name
    }

}
