//
//  ViewController.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

import UIKit
import Hydra

class SchoolListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var schoolList: [School]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "School List"
        fetchSchools()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /// This funciton will fetch the school data from the API
    func fetchSchools() {
        //1. Create the Service Configuration
        let config = ServiceConfig.appConfig()
        let service = Service(config!)
        //2. Fetch the School details from the API
        //Given more time this can be wrapped in the VIPER Model and the below code will go into the PRESENTER class which in turn calls the Interactor class to call the API.
        FetchSchools().execute(in: service).then { [weak self] schools in
            self?.schoolList = schools
            self?.tableView.reloadData()
        }.catch({ err in
            
        })
    }
}

extension SchoolListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let schools = schoolList else {
            return 1
        }
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "SchoolCell") as! SchoolTableViewCell
        guard let schools = schoolList else {
            cell.lblName.text = "No school Data Found"
            return cell
        }
        
        let school = schools[indexPath.row]
        
        cell.school = school
        cell.setup()
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let schools = schoolList else {
            return
        }
        //Given more time, this can be wrapped with the VIPER Pattern and the below code will go to the router class.
        let school = schools[indexPath.row]
        let schoolDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "SchoolDetailViewController") as! SchoolDetailViewController
        schoolDetailVC.school = school
        self.navigationController?.pushViewController(schoolDetailVC, animated: true)
    }
    
}

