//
//  School.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

import Foundation
import SwiftyJSON

class School {
    let id: String
    let email: String
    let website: String
    let name: String
    
    init(_ json: JSON) {
        self.id = json["dbn"].stringValue
        self.email = json["school_email"].stringValue
        self.name = json["school_name"].stringValue
        self.website = json["website"].stringValue
    }
    
    static func load(list:[JSON]) -> [School] {
        return list.compactMap { School($0) }
    }
}
