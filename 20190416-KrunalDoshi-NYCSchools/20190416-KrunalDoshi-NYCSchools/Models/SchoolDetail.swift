//
//  File.swift
//  20190416-KrunalDoshi-NYCSchools
//
//  Created by Keyur Doshi on 4/16/19.
//  Copyright © 2019 Krunal Doshi. All rights reserved.
//

import Foundation
import SwiftyJSON

class SchoolDetail {
    let id: String
    let numberOfSatTaker: String
    let criticalReadingAvgScore: String
    let mathAvgScore: String
    let writingAvgScore: String
    
    init(_ json: JSON) {
        let responseArray = json.arrayValue
        if !responseArray.isEmpty {
            let dict = responseArray[0]
            self.id = dict["dbn"].stringValue
            self.numberOfSatTaker = dict["num_of_sat_test_takers"].stringValue
            self.criticalReadingAvgScore = dict["sat_critical_reading_avg_score"].stringValue
            self.mathAvgScore = dict["sat_math_avg_score"].stringValue
            self.writingAvgScore = dict["sat_writing_avg_score"].stringValue
        } else {
            self.id = json["dbn"].stringValue
            self.numberOfSatTaker = json["num_of_sat_test_takers"].stringValue
            self.criticalReadingAvgScore = json["sat_critical_reading_avg_score"].stringValue
            self.mathAvgScore = json["sat_math_avg_score"].stringValue
            self.writingAvgScore = json["sat_writing_avg_score"].stringValue
        }
    }
}
