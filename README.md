# 20190416-KrunalDoshi-NYCSchools

This Project list the average SAT score for NYC Schools.

## How to test the app
Clone the repo - `git clone https://gitlab.com/KrunalDoshi/20190416-krunaldoshi-nycschools.git`

Open the Workspace in the Xcode

Run the app in the Simulator

## Minimum Requirements
- Swift - 4.2
- Xcode - 10+

## Screenshots
![](Screenshots/SchoolList.png)
![](Screenshots/SchoolDetails.png)

## API Endpoints
School Lists - `https://data.cityofnewyork.us/resource/s3k6-pzi2.json`

School SAT Score - `https://data.cityofnewyork.us/resource/f9bf-2cp4.json`

## Libraries Used
Networking Support - In this application to make the API request [AlamoFire](https://github.com/Alamofire/Alamofire). Switching to NSURLSession or any other library is quite easy as the structure of the network layer is robust.

Promise/Future Support - The Network layer in the app uses the [Hydra](https://github.com/malcommac/Hydra) to give the promise support which is clean, simple and gives strong error handling mechanism.

SwiftyJSON - This Network layer uses the [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) to deal with the JSON Data. And easily convert the raw json data into models.

## Improvements
- Add Pagination using offset and limit parameters
- Search/Filter Functionality
- Caching the API Responses
- More graceful Error Handling
- Unit Test Cases



